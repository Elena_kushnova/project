$(document).ready(function(){
    $('.slider').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
  })

  $('.last-slider-1').slick({
      dots: false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToShow: 5,
      slidesToScroll: 1,
      centerMode: true,
      arrows: false,
  })

  $('.last-slider').slick({
      dots: false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: false,
  })

  $(".accordion p").hide();
  $(".accordion h3").click(function () {
   $(this).next("p").slideToggle("slow").siblings("p:visible").slideUp("slow");
   $(this).toggleClass("active");
   $(".accordion p").addClass("active-p");
   $(this).siblings().removeClass("active");
  });

})
